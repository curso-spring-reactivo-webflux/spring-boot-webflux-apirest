package com.luinel.springboot.webflux.app;

import com.luinel.springboot.webflux.app.models.documents.Categoria;
import com.luinel.springboot.webflux.app.models.documents.Producto;
import com.luinel.springboot.webflux.app.models.services.ProductoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import reactor.core.publisher.Flux;

import java.util.Date;

@EnableEurekaClient
@SpringBootApplication
public class SpringBootWebfluxApirestApplication implements CommandLineRunner {

	@Autowired
	private ProductoService service;

	@Autowired
	private ReactiveMongoTemplate mongoTemplate;

	private static final Logger log = LoggerFactory.getLogger(SpringBootWebfluxApirestApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringBootWebfluxApirestApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		mongoTemplate.dropCollection("productos").subscribe();
		mongoTemplate.dropCollection("categorias").subscribe();

		Categoria electronica = new Categoria("Electrónica");
		Categoria deporte = new Categoria("Deporte");
		Categoria computacion = new Categoria("Computación");
		Categoria muebles = new Categoria("Muebles");

		Flux.just(electronica, deporte, computacion, muebles)
				.flatMap(c -> service.saveCategoria(c))
				.doOnNext( c -> log.info("Categoria creada: "+c.getNombre()+" ID: "+c.getId()))
				.thenMany(
						Flux.just(
										new Producto("TV Panasonic Pantalla LCD", 456.89, electronica),
										new Producto("Sony Camara HD Digital",177.89, electronica),
										new Producto("Apple Ipod",46.89, electronica),
										new Producto("Sony Notebook",846.89, computacion),
										new Producto("HP Multifuncional",200.89, computacion),
										new Producto("Bianchi Bicicleta",70.89, deporte),
										new Producto("HP Notebook Omen 17",2500.89, computacion),
										new Producto("Mica Cómoda 5 Cajones",150.89, muebles),
										new Producto("TV Sony Bravia OLED 4k",2255.89, electronica)
								)
								.flatMap(producto -> {
									producto.setCreateAt(new Date());
									return service.save(producto);
								})
				)
				.subscribe(producto -> log.info("Insert: " + producto.getId() + " " + producto.getNombre()));
	}
}
