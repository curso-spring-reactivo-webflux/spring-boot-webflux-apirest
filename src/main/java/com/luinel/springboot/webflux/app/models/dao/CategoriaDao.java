package com.luinel.springboot.webflux.app.models.dao;

import com.luinel.springboot.webflux.app.models.documents.Categoria;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface CategoriaDao extends ReactiveMongoRepository<Categoria,String> {

    public Mono<Categoria> findByNombre(String nombre);
}
