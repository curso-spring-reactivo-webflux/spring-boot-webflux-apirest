package com.luinel.springboot.webflux.app.models.dao;

import com.luinel.springboot.webflux.app.models.documents.Producto;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface ProductoDao extends ReactiveMongoRepository<Producto,String> {

    public Mono<Producto> findByNombre(String nombre);

}
